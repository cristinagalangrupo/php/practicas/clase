<?php
if (isset($_REQUEST["calcular"])) {
            $resultado=[
                "media"=>"",
                "moda"=>"",
                "mediana"=>"",
                "desviacion"=>""
            ];
            $numeros=$_REQUEST["numeros"];
            $aux = $numeros;
            sort($aux);
            //var_dump($aux);
            /* Quito los vacíos*/
             foreach ($aux as $value) {
                    if($value!=""){
                        $ordenado[]=$value;
                    }
                } 
                //var_dump($ordenado);
                $n=count($ordenado);
            
            
            /*  ********** MEDIA  ********** */
            $suma= array_sum($numeros);
            $media=$suma/$n;
            $resultado["media"]=$media;
            
             /*  ********** MEDIANA  ********** */
            if($n%2==0){
                $indice=$n/2;
                //var_dump($n);
                //var_dump($indice);
                //var_dump($ordenado[$indice]);
                $mediana=($ordenado[$indice-1]+$ordenado[$indice])/2;
                
            }else{
               
               $indice=($n+1)/2;
               $mediana=$ordenado[$indice-1];
            }
            $resultado["mediana"]=$mediana;
            /*  ********** DESVIACIÓN TÍPICA **********  */
            
          
            $w=0;
            
            foreach ($ordenado as $v) {
                $w+=pow(($v-$media),2);
            }
            //var_dump($w); /* 1071.77*/ 
            $dentro=$w/$n;
            
            $desviacion= sqrt($dentro);
            $resultado["desviacion"]=$desviacion;
            /*  ********** MODA  ********** */
            $miarray=[];
            //var_dump($miarray);
           
            foreach ($ordenado as $value) {
                $miarray[$value]=0;
            }
            foreach ($ordenado as $value) {
                $miarray[$value]=$miarray[$value]+1;
            }
            
            //var_dump($miarray);
            $maximo=max($miarray);
            foreach ($miarray as $indice=>$v){
                if($v==$maximo){
                    $moda=$indice;
                }
            }
            $resultado["moda"]=$moda;
            
            
            $datos=[
                "titulo"=>"Los resultados son",
                "content"=>"resultados.php",
                "mensaje"=>"si quieres probar de nuevo",
                "pie"=> "Cristina Galán",
                "resultado"=>$resultado,
            ];
        }else{
            $datos=[
                "titulo"=>"Introduce los datos",
                "content"=>"formulario.php",
                "pie"=> "Cristina Galán"
            ];
        }



