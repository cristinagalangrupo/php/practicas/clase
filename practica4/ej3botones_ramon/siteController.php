<?php

function actionMedia(){
    $datos=$_REQUEST['numeros'];
    /**
     * compruebo si es un array o un string
     */
    if(gettype($datos)=="string"){
        
        $datos=explode(",",$datos);
        
    }
    $resultado=0;
    foreach ($datos as $v) {
        $resultado+=$v;
    }
    $resultado/=count($datos);
    /*
     * antes de mandar a la vista los números los convierto en un string
     */
    
    $datos=implode(",",$datos);
    return[
        "vista"=>"resultado.php",
        "mensaje"=>"La media calculada es: ",
        "resultado"=>$resultado,
        "numeros"=>$datos
        ];
}
function actionModa(){
    $datos=$_REQUEST['numeros'];
    /**
     * compruebo si es un array o un string
     */
    if(gettype($datos)=="string"){
        $datos=explode(",",$datos);
    }
    $resultado=0;
    $miarray=[];
    
    foreach ($datos as $value) {
                $miarray[$value]=0;
            }
    foreach ($datos as $value) {
                $miarray[$value]=$miarray[$value]+1;
            }
            
            $maximo=max($miarray);
            
            foreach ($miarray as $indice=>$v){
                if($v==$maximo){
                    $resultado=$indice;
                }
            
 }
     $datos=implode(",",$datos);      
    return[
        "vista"=>"resultado.php",
        "mensaje"=>"La moda calculada es: ",
        "resultado"=>$resultado,
        "numeros"=>$datos,
    ];
}
function actionMediana(){
    $datos=$_REQUEST['numeros'];
    /**
     * compruebo si es un array o un string
     */
    if(gettype($datos)=="string"){
        $datos=explode(",",$datos);
    }
    $resultado=0;
    $aux = $datos;
    
            sort($aux);
            /* Quito los vacíos*/
             foreach ($aux as $value) {
                    if($value!=""){
                        $ordenado[]=$value;
                    }
                } 
     $n=count($ordenado);
    if($n%2==0){
                $indice=$n/2;
                $resultado=($ordenado[$indice-1]+$ordenado[$indice])/2;
                
            }else{
               $indice=($n+1)/2;
               $resultado=$ordenado[$indice-1];
            }
    
    $datos=implode(",",$datos);
    return[
        "vista"=>"resultado.php",
        "mensaje"=>"La mediana calculada es: ",
        "resultado"=>$resultado,
        "numeros"=>$datos
    ];
}
function actionDesviacion(){
    $datos=$_REQUEST['numeros'];
    /**
     * compruebo si es un array o un string
     */
    if(gettype($datos)=="string"){
        $datos=explode(",",$datos);
    }
    $resultado=0;
    
    $arr=actionMedia();
        $media=$arr["resultado"];
          
         $nums=$datos;
         $aux = $datos;
            sort($aux);
            /* Quito los vacíos*/
             foreach ($aux as $value) {
                    if($value!=""){
                        $ordenado[]=$value;
                    }
                } 
     $n=count($ordenado);
     $w=0;
            foreach ($ordenado as $v) {
                $w+=pow(($v-$media),2);
            }
           
            $dentro=$w/$n;
            $resultado= sqrt($dentro);
         
    $datos=implode(",",$datos);
    return[
        "vista"=>"resultado.php",
        "mensaje"=>"La desviación típica calculada es: ",
        "resultado"=>$resultado,
        "numeros"=>$datos
    ];
}
function actionFormulario(){
return[
        "vista"=>"formulario.php"    
    ];
}
          
if(isset($_REQUEST["boton"])){
    switch ($_REQUEST["boton"]){
        case "media":
            $datos=actionMedia();
            break;
        case "moda":
            $datos=actionModa();
            break;
        case "mediana":
            $datos= actionMediana();
            break;
        case "desviacion":
            $datos= actionDesviacion();
            break;
        default:
            $datos=actionFormulario();
    }
}else{
    $datos= actionFormulario();
}