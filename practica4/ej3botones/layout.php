<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title><?= $datos["titulo"]; ?></title>
    <style type="text/css">
        div{
            margin:0px 30px;
        }
        label{
            margin:10px;
        }
        .formulario{
            height: 200px;
            width: 100%;
            margin:50px;
        }
        .solucion{
            border:3px solid red;
            line-height: 50px;
            background-color: pink;
            color:red;
            font-weight: bold;
            text-align:center;
            font-size:2em;
            margin:50px;
              
            
        }
        .solucion li{
            list-style-type: none;
        }
        label{
            font-weight: bold;
        }
        .izquierda,.derecha{
            float:left
        }
       
    </style>
  </head>
  <body>
      <form>      
<nav class="navbar navbar-expand-lg navbar-light bg-info">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
          <button class="nav-link " name="introduce">Introduce los datos</button>
      </li>
      <li class="nav-item"><button class="nav-link" name="media">Media</button></li>
       <li class="nav-item">
          <button class="nav-link" name="moda">Moda</button>
      </li>
       <li class="nav-item">
           <button class="nav-link" name="mediana">Mediana</button>
      </li>
      <li class="nav-item">
          <button class="nav-link" name="desviacion">Desviación típica</button>
      </li>
    </ul>
  </div>
</nav>
        <?php
        include $datos["content"];
        ?>
      </form>
      
        

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html> 