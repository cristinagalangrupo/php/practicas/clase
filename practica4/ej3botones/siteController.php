<?php
    
function actionMedia(){
    $numeros=$_REQUEST["numeros"];
    $nums=[
        "n1"=>$numeros[0],
        "n2"=>$numeros[1],
        "n3"=>$numeros[2],
        "n4"=>$numeros[3],
        "n5"=>$numeros[4],
        "n6"=>$numeros[5],
        "n7"=>$numeros[6],
        "n8"=>$numeros[7],
        "n9"=>$numeros[8],
        "n10"=>$numeros[9],
      ];
    
    //var_dump($numeros);
            $aux = $numeros;
            sort($aux);
            /* Quito los vacíos*/
             foreach ($aux as $value) {
                    if($value!=""){
                        $ordenado[]=$value;
                    }
                } 
     $n=count($ordenado);
     
    $suma= array_sum($ordenado);
            $media=$suma/$n;
    return [
        "mensaje"=>"El resultado de la media es: ",
        "content"=>"media.php",
        "resultado"=>$media,
        "numeros"=>$nums,
        "titulo"=>"Media",
    ];
}   
function actionModa(){
        $numeros=$_REQUEST["numeros"];
         $nums=[
        "n1"=>$numeros[0],
        "n2"=>$numeros[1],
        "n3"=>$numeros[2],
        "n4"=>$numeros[3],
        "n5"=>$numeros[4],
        "n6"=>$numeros[5],
        "n7"=>$numeros[6],
        "n8"=>$numeros[7],
        "n9"=>$numeros[8],
        "n10"=>$numeros[9],
      ];
            $aux = $numeros;
            sort($aux);
            /* Quito los vacíos*/
             foreach ($aux as $value) {
                    if($value!=""){
                        $ordenado[]=$value;
                    }
                } 
    $miarray=[];
            //var_dump($miarray);
           
            foreach ($ordenado as $value) {
                $miarray[$value]=0;
            }
            foreach ($ordenado as $value) {
                $miarray[$value]=$miarray[$value]+1;
            }
            
            //var_dump($miarray);
            $maximo=max($miarray);
            foreach ($miarray as $indice=>$v){
                if($v==$maximo){
                    $moda=$indice;
                }
            }
           
    return [
        "mensaje"=>"El resultado de la moda es: ",
        "content"=>"moda.php",
        "resultado"=>$moda,
        "numeros"=>$nums,
        "titulo"=>"Moda",
    ];
}
function actionMediana(){
        $numeros=$_REQUEST["numeros"];
         $nums=[
        "n1"=>$numeros[0],
        "n2"=>$numeros[1],
        "n3"=>$numeros[2],
        "n4"=>$numeros[3],
        "n5"=>$numeros[4],
        "n6"=>$numeros[5],
        "n7"=>$numeros[6],
        "n8"=>$numeros[7],
        "n9"=>$numeros[8],
        "n10"=>$numeros[9],
      ];
            $aux = $numeros;
            sort($aux);
            /* Quito los vacíos*/
             foreach ($aux as $value) {
                    if($value!=""){
                        $ordenado[]=$value;
                    }
                } 
     $n=count($ordenado);
        $numeros=$_REQUEST["numeros"];
        
            
     $n=count($ordenado);
     
    if($n%2==0){
                $indice=$n/2;
                //var_dump($n);
                //var_dump($indice);
                //var_dump($ordenado[$indice]);
                //var_dump($ordenado);
                $mediana=($ordenado[$indice-1]+$ordenado[$indice])/2;
                
            }else{
               $indice=($n+1)/2;
               $mediana=$ordenado[$indice-1];
            }
    return [
        "mensaje"=>"El resultado de la mediana es: ",
        "content"=>"mediana.php",
        "resultado"=>$mediana,
        "numeros"=>$nums,
        "titulo"=>"Mediana",
    ];
}
function actionDesviacion(){
        $arr=actionMedia();
        $media=$arr["resultado"];
        //var_dump($media);
        $numeros=$_REQUEST["numeros"];
        
         $nums=$numeros;
                 /*[
        "n1"=>$numeros[0],
        "n2"=>$numeros[1],
        "n3"=>$numeros[2],
        "n4"=>$numeros[3],
        "n5"=>$numeros[4],
        "n6"=>$numeros[5],
        "n7"=>$numeros[6],
        "n8"=>$numeros[7],
        "n9"=>$numeros[8],
        "n10"=>$numeros[9],
      ];*/
            $aux = $numeros;
            sort($aux);
            /* Quito los vacíos*/
             foreach ($aux as $value) {
                    if($value!=""){
                        $ordenado[]=$value;
                    }
                } 
     $n=count($ordenado);
     $w=0;
            foreach ($ordenado as $v) {
                $w+=pow(($v-$media),2);
            }
            //var_dump($w); /* 1071.77*/ 
            $dentro=$w/$n;
            $desviacion= sqrt($dentro);
           
    return [
        "mensaje"=>"El resultado de la desviación típica es: ",
        "content"=>"desviacion.php",
        "resultado"=>$desviacion,
        "numeros"=>$nums,
        "titulo"=>"Desviación típica",
    ];
}
 
function actionIndex(){
return[
    "titulo"=>"Funciones estadísticas",
    "content"=>"formulario.php"    
    ];
}
          
if(isset($_REQUEST["media"])){
       $datos=actionMedia();
       
}elseif(isset($_REQUEST["moda"])){
    $datos=actionModa();
}elseif(isset($_REQUEST["mediana"])){
    $datos=actionMediana();
}elseif(isset($_REQUEST["desviacion"])){
    $datos= actionDesviacion();
}else{
    $datos= actionIndex();
}